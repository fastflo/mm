#!/usr/bin/python

import sys
import os
import re
import time
import cPickle
import bz2

next_interpret_id = 0

interprets = {} # name -> interpret-id
db = dict() # interpretA-id -> { interpretB-id: score }
urls = {} # interpret-id -> url

def add_score(ia, ib, score):
    # print "add score %r <- %f -> %r" % (ia, score, ib)
    iai = interprets[ia]
    ibi = interprets[ib]
    if iai not in db:
        db[iai] = dict()
    if ibi not in db:
        db[ibi] = dict()
    if False:
        existing_score = db[iai].get(ibi)
        if existing_score is not None and existing_score != score:
            print "warning: %r to %r has existing score %r vs. new score %r" % (
                ia, ib, existing_score, score)
    db[iai][ibi] = score
    db[ibi][iai] = score

def read_file(fn, fp=None):
    global next_interpret_id
    if fp is None:
        #print "reading %r..." % fn
        with open(fn, "rb") as fp:
            return read_file(fn, fp)
    data = fp.read()
    gnod = re.search("<div id=gnodMap>(.*)</div>", data, re.S)
    if not gnod:
        print "%s: no gnod-map!" % fn        
        return
    gnod = gnod.group(1)
    smap = dict()
    for url, iid, i in re.findall('<a href="(.*?)" class=S id=(.*?)>(.*?)</a>', gnod):
        i = i.decode("latin1")
        interpret_id = interprets.get(i)
        if interpret_id is None:
            interpret_id = interprets[i] = next_interpret_id
            next_interpret_id += 1
        if iid == "s0":
            urls[interpret_id] = url
        smap[iid] = i
    if not smap:
        print "%s: no smap entries!" % fn
        return
    had_scores = False
    n_scores = 0
    for a, scores in re.findall('Aid\[([0-9]+)\]=new Array\((.*?)\);', data, re.S):
        sa = "s" + a
        ia = smap[sa]
        if ia.count("?") == len(ia): continue
        scores = scores.split(",")
        for b, score in enumerate(scores):
            if score == "-1":
                continue
            sb = "s%d" % b
            ib = smap[sb]
            if ib.count("?") == len(ib): continue
            add_score(ia, ib, float(score))
            n_scores += 1
        had_scores = True
        break # only use s0 info
    if not had_scores:
        print "%s: no scores found!" % fn
        return
    #print "%s: added %d scores" % (fn, n_scores)
    
def read_all(dn):    
    files = []
    for e in os.listdir(dn):        
        if e[0] == "." or not e.endswith(".html"):
            continue
        files.append(e)
    files.sort()
    print "%d files" % len(files)
    start = time.time()
    for f in files:
        read_file(os.path.join(dn, f))

        now = time.time()
        if now - start > 1:
            start = now
            print "%d interprets, %d urls" % (len(db), len(urls))
            
if __name__ == "__main__":
    if len(sys.argv) > 1:
        dn = sys.argv[1]
    else:
        dn = "."
    read_all(dn)

    out = "db.bz2"
    data = dict(
        db=db,
        urls=urls,
        interprets=interprets
    )
    with bz2.BZ2File(out, "w", compresslevel=9) as fp:
        cPickle.dump(data, fp, protocol=-1)
    
    
